package arkell.ru.sovkombankfive;

import org.junit.Test;

import java.util.ArrayList;

import arkell.ru.sovkombankfive.presenter.MainPresenter;
import arkell.ru.sovkombankfive.utils.Generator;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class SovkomBankUnitTest {

    @Test
    public void testStringGenerator() {
        System.out.println(new Generator().generate(50, true));
    }

    @Test
    public void amountOfStrings() {
        ArrayList<String> result = new MainPresenter().generateString(10000);
        for (int i = 0; i < result.size(); i++) {
            System.out.println(result.get(i));
        }
    }
}