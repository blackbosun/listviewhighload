package arkell.ru.sovkombankfive.interfaces;

public interface Generatable {
    String generate(int length, boolean upperCase);
}
