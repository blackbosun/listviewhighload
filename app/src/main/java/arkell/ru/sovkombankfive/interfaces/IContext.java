package arkell.ru.sovkombankfive.interfaces;

import android.content.Context;

public interface IContext {
    Context getContext();
}
