package arkell.ru.sovkombankfive;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import arkell.ru.sovkombankfive.interfaces.IChangeFragment;
import arkell.ru.sovkombankfive.interfaces.IContext;
import arkell.ru.sovkombankfive.utils.Constants;
import arkell.ru.sovkombankfive.view.MainFragment;

public class MainActivity extends AppCompatActivity implements IChangeFragment, IContext {

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentChangeChecking(savedInstanceState);
    }

    /**
     * Change fragment on new when argument is null
     *
     * @param savedInstanceState - bundle with saved information
     */
    private void fragmentChangeChecking(Bundle savedInstanceState) {
        try {
            savedInstanceState.getBoolean(Constants.SHOULD_CHANGE_FRAGMENT);
        } catch (NullPointerException e) {
            changeFragment(new MainFragment(this));
        }
    }

    @Override
    public void changeFragment(android.support.v4.app.Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
    }

    @Override
    public Context getContext() {
        return this;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(Constants.SHOULD_CHANGE_FRAGMENT, false);
    }
}
