package arkell.ru.sovkombankfive.utils;

import arkell.ru.sovkombankfive.interfaces.Generatable;

/**
 * Works with generating data.
 */

public class Generator implements Generatable {
    private char[] sources = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
            'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ' '};

    /**
     * Generate string. Might be lowercase only or with uppercase letters, depending on upperCase value;
     *
     * @param length    - length of string to generate
     * @param upperCase - if true, randomly create uppercase letters. Else create only lowercase
     * @return generated string;
     */
    @Override
    public String generate(int length, boolean upperCase) {
        StringBuilder result = new StringBuilder();

        int sourcesLength = sources.length;

        if (upperCase) {
            for (int i = 0; i < length; i++) {
                int shift = (int) (Math.random() * sourcesLength);
                byte upperCaseWord = (byte) (Math.random() * 10);
                boolean isUpperCase = upperCaseWord > 5;
                if (isUpperCase) {
                    String latter = String.valueOf(sources[shift]).toUpperCase();
                    result.append(latter);
                } else {
                    result.append(sources[shift]);
                }
            }
        } else {
            for (int i = 0; i < length; i++) {
                int shift = (int) (Math.random() * sourcesLength);
                result.append(sources[shift]);
            }
        }
        return String.valueOf(result);
    }
}
