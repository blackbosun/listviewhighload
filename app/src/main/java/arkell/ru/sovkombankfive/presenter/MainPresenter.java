package arkell.ru.sovkombankfive.presenter;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import arkell.ru.sovkombankfive.interfaces.Generatable;
import arkell.ru.sovkombankfive.utils.Generator;

public class MainPresenter {

    private Generatable generator = new Generator();

    public ArrayList<String> generateString(int amount) {
        ArrayList<String> strings = new ArrayList<>();
        for (int i = 0; i < amount; i++) {
            strings.add(generator.generate(30, true));
        }
        return strings;
    }

    public ArrayList<String> getStringFromBundle(Bundle savedInstanceState) {
        try {
            return savedInstanceState.getStringArrayList("STRINGS");
        } catch (NullPointerException e) {
            return generateString(10000);
        }
    }
}
