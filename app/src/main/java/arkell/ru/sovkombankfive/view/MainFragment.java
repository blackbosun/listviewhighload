package arkell.ru.sovkombankfive.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import arkell.ru.sovkombankfive.R;
import arkell.ru.sovkombankfive.interfaces.IContext;
import arkell.ru.sovkombankfive.presenter.MainPresenter;

public class MainFragment extends Fragment {
    private IContext iContext;
    private ArrayList<String> strings = new ArrayList<>();
    private MainPresenter presenter = new MainPresenter();


    @SuppressLint("ValidFragment")
    public MainFragment(IContext iContext) {
        this.iContext = iContext;
    }

    public MainFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.main_fragment, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeView(view, savedInstanceState);

    }

    private void initializeView(View view, Bundle savedInstanceState) {
        strings = presenter.getStringFromBundle(savedInstanceState);
        ListView listView = view.findViewById(R.id.lvmain);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(iContext.getContext(), R.layout.rv_item, strings);
        listView.setAdapter(adapter);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList("STRINGS", strings);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        setRetainInstance(true);
    }
}
